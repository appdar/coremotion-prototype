//
//  ViewController.swift
//  CoreMotionPrototype
//
//  Created by SJ Singh on 5/11/15.
//  Copyright (c) 2015 SJ Singh. All rights reserved.
//

import UIKit
import CoreMotion

class ViewController: UIViewController {
    
    @IBOutlet weak var updateLabel: UILabel!
    
    let motionActivityManager = CMMotionActivityManager()
    let motionManager = CMMotionManager()
    let motionHandlerQueue = NSOperationQueue()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //if CMMotionActivityManager.isActivityAvailable() {
            motionActivityManager.startActivityUpdatesToQueue(motionHandlerQueue, withHandler: { (activity: CMMotionActivity!) -> Void in
                
                //if (activity.walking){
                    self.startLogging()
                //}
                
            })
            
        //}else{
        //    NSLog("ActivityManager not available")
        //}
    }
    
    func startLogging(){
        motionManager.accelerometerUpdateInterval = 1.0
        motionManager.startAccelerometerUpdatesToQueue(motionHandlerQueue, withHandler: { (data: CMAccelerometerData!, error:NSError!) -> Void in
            self.appendText("Accelerometer x: \(data.acceleration.x)\nAccelerometer y: \(data.acceleration.y)\nAccelerometer z: \(data.acceleration.z)")
            
        })
    }

    func appendText(message: String)
    {

        
        dispatch_async(dispatch_get_main_queue(), {
            self.updateLabel.text = message
        })
        
    }


}

